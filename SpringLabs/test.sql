-- HIBERNATE    
    select example.firstName, example.lastName, nickname.nickName 
      from Example example   
inner join Nickname nickname 
      with example.id = nickname.id 
        
-- RAW SQL
    select example0_.first_name as col_0_0_,
           example0_.last_name as col_1_0_,
           nickname1_.nick_name as col_2_0_ 
      from EXAMPLES example0_ 
inner join NICKNAMES nickname1_ 
        on (example0_.id = nickname1_.id);