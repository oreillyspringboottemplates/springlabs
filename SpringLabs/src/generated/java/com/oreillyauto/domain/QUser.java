package com.oreillyauto.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUser is a Querydsl query type for User
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUser extends EntityPathBase<User> {

    private static final long serialVersionUID = 422217595L;

    public static final QUser user = new QUser("user");

    public final DateTimePath<java.sql.Timestamp> birthDate = createDateTime("birthDate", java.sql.Timestamp.class);

    public final StringPath department = createString("department");

    public final StringPath firstName = createString("firstName");

    public final StringPath lastName = createString("lastName");

    public final NumberPath<Integer> userId = createNumber("userId", Integer.class);

    public final ListPath<com.oreillyauto.domain.examples.UserRole, com.oreillyauto.domain.examples.QUserRole> userroleList = this.<com.oreillyauto.domain.examples.UserRole, com.oreillyauto.domain.examples.QUserRole>createList("userroleList", com.oreillyauto.domain.examples.UserRole.class, com.oreillyauto.domain.examples.QUserRole.class, PathInits.DIRECT2);

    public QUser(String variable) {
        super(User.class, forVariable(variable));
    }

    public QUser(Path<? extends User> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUser(PathMetadata metadata) {
        super(User.class, metadata);
    }

}

