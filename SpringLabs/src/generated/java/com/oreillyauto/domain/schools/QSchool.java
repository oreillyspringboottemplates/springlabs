package com.oreillyauto.domain.schools;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSchool is a Querydsl query type for School
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSchool extends EntityPathBase<School> {

    private static final long serialVersionUID = 1887875187L;

    public static final QSchool school = new QSchool("school");

    public final NumberPath<Integer> schoolId = createNumber("schoolId", Integer.class);

    public final StringPath schoolName = createString("schoolName");

    public final ListPath<Student, QStudent> studentList = this.<Student, QStudent>createList("studentList", Student.class, QStudent.class, PathInits.DIRECT2);

    public QSchool(String variable) {
        super(School.class, forVariable(variable));
    }

    public QSchool(Path<? extends School> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSchool(PathMetadata metadata) {
        super(School.class, metadata);
    }

}

