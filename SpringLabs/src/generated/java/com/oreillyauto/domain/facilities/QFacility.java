package com.oreillyauto.domain.facilities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QFacility is a Querydsl query type for Facility
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFacility extends EntityPathBase<Facility> {

    private static final long serialVersionUID = -901273088L;

    public static final QFacility facility = new QFacility("facility");

    public final NumberPath<Integer> facilityId = createNumber("facilityId", Integer.class);

    public final StringPath facilityName = createString("facilityName");

    public final ListPath<Teammember, QTeammember> teammemberList = this.<Teammember, QTeammember>createList("teammemberList", Teammember.class, QTeammember.class, PathInits.DIRECT2);

    public QFacility(String variable) {
        super(Facility.class, forVariable(variable));
    }

    public QFacility(Path<? extends Facility> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFacility(PathMetadata metadata) {
        super(Facility.class, metadata);
    }

}

