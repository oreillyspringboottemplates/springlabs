package com.oreillyauto.domain.facilities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTeammember is a Querydsl query type for Teammember
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTeammember extends EntityPathBase<Teammember> {

    private static final long serialVersionUID = 157985620L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTeammember teammember = new QTeammember("teammember");

    public final QFacility facility;

    public final StringPath firstName = createString("firstName");

    public final StringPath lastName = createString("lastName");

    public final NumberPath<Integer> teammemberId = createNumber("teammemberId", Integer.class);

    public QTeammember(String variable) {
        this(Teammember.class, forVariable(variable), INITS);
    }

    public QTeammember(Path<? extends Teammember> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTeammember(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTeammember(PathMetadata metadata, PathInits inits) {
        this(Teammember.class, metadata, inits);
    }

    public QTeammember(Class<? extends Teammember> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.facility = inits.isInitialized("facility") ? new QFacility(forProperty("facility")) : null;
    }

}

