package com.oreillyauto.domain.interns;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QIntern is a Querydsl query type for Intern
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QIntern extends EntityPathBase<Intern> {

    private static final long serialVersionUID = 1574431151L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QIntern intern = new QIntern("intern");

    public final QBadge badge;

    public final NumberPath<Integer> internId = createNumber("internId", Integer.class);

    public final StringPath internName = createString("internName");

    public QIntern(String variable) {
        this(Intern.class, forVariable(variable), INITS);
    }

    public QIntern(Path<? extends Intern> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QIntern(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QIntern(PathMetadata metadata, PathInits inits) {
        this(Intern.class, metadata, inits);
    }

    public QIntern(Class<? extends Intern> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.badge = inits.isInitialized("badge") ? new QBadge(forProperty("badge"), inits.get("badge")) : null;
    }

}

