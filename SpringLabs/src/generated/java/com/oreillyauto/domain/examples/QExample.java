package com.oreillyauto.domain.examples;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QExample is a Querydsl query type for Example
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QExample extends EntityPathBase<Example> {

    private static final long serialVersionUID = -290730955L;

    public static final QExample example = new QExample("example");

    public final StringPath firstName = createString("firstName");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath lastName = createString("lastName");

    public QExample(String variable) {
        super(Example.class, forVariable(variable));
    }

    public QExample(Path<? extends Example> path) {
        super(path.getType(), path.getMetadata());
    }

    public QExample(PathMetadata metadata) {
        super(Example.class, metadata);
    }

}

