package com.oreillyauto.domain.examples;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QNickname is a Querydsl query type for Nickname
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QNickname extends EntityPathBase<Nickname> {

    private static final long serialVersionUID = 2005354243L;

    public static final QNickname nickname = new QNickname("nickname");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath nickName = createString("nickName");

    public QNickname(String variable) {
        super(Nickname.class, forVariable(variable));
    }

    public QNickname(Path<? extends Nickname> path) {
        super(path.getType(), path.getMetadata());
    }

    public QNickname(PathMetadata metadata) {
        super(Nickname.class, metadata);
    }

}

