package com.oreillyauto.domain.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPlanner is a Querydsl query type for Planner
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPlanner extends EntityPathBase<Planner> {

    private static final long serialVersionUID = 74438606L;

    public static final QPlanner planner = new QPlanner("planner");

    public final StringPath firstName = createString("firstName");

    public final StringPath lastName = createString("lastName");

    public final NumberPath<Integer> plannerId = createNumber("plannerId", Integer.class);

    public final SetPath<Project, QProject> projects = this.<Project, QProject>createSet("projects", Project.class, QProject.class, PathInits.DIRECT2);

    public QPlanner(String variable) {
        super(Planner.class, forVariable(variable));
    }

    public QPlanner(Path<? extends Planner> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPlanner(PathMetadata metadata) {
        super(Planner.class, metadata);
    }

}

