package com.oreillyauto.domain.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProject is a Querydsl query type for Project
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProject extends EntityPathBase<Project> {

    private static final long serialVersionUID = 259014933L;

    public static final QProject project = new QProject("project");

    public final SetPath<Planner, QPlanner> employees = this.<Planner, QPlanner>createSet("employees", Planner.class, QPlanner.class, PathInits.DIRECT2);

    public final NumberPath<Integer> projectId = createNumber("projectId", Integer.class);

    public final StringPath title = createString("title");

    public QProject(String variable) {
        super(Project.class, forVariable(variable));
    }

    public QProject(Path<? extends Project> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProject(PathMetadata metadata) {
        super(Project.class, metadata);
    }

}

