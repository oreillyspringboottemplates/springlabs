package com.oreillyauto.domain.foo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPupil is a Querydsl query type for Pupil
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPupil extends EntityPathBase<Pupil> {

    private static final long serialVersionUID = 466589846L;

    public static final QPupil pupil = new QPupil("pupil");

    public final NumberPath<Integer> classId = createNumber("classId", Integer.class);

    public final DateTimePath<java.sql.Timestamp> enrollDate = createDateTime("enrollDate", java.sql.Timestamp.class);

    public final StringPath firstName = createString("firstName");

    public final StringPath lastName = createString("lastName");

    public final NumberPath<Integer> txId = createNumber("txId", Integer.class);

    public QPupil(String variable) {
        super(Pupil.class, forVariable(variable));
    }

    public QPupil(Path<? extends Pupil> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPupil(PathMetadata metadata) {
        super(Pupil.class, metadata);
    }

}

