package com.oreillyauto.domain.foo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QClazz is a Querydsl query type for Clazz
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QClazz extends EntityPathBase<Clazz> {

    private static final long serialVersionUID = 454302080L;

    public static final QClazz clazz = new QClazz("clazz");

    public final NumberPath<Integer> classId = createNumber("classId", Integer.class);

    public final StringPath className = createString("className");

    public final StringPath days = createString("days");

    public final StringPath duration = createString("duration");

    public final StringPath instructor = createString("instructor");

    public final ListPath<Pupil, QPupil> pupilList = this.<Pupil, QPupil>createList("pupilList", Pupil.class, QPupil.class, PathInits.DIRECT2);

    public final StringPath start = createString("start");

    public QClazz(String variable) {
        super(Clazz.class, forVariable(variable));
    }

    public QClazz(Path<? extends Clazz> path) {
        super(path.getType(), path.getMetadata());
    }

    public QClazz(PathMetadata metadata) {
        super(Clazz.class, metadata);
    }

}

