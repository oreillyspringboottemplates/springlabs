
package com.oreillyauto.model.currency;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "primary", "type", "currency", "balance", "created_at", "updated_at", "resource",
		"resource_path", "allow_deposits", "allow_withdrawals" })
public class Datum implements Serializable {
	private final static long serialVersionUID = 3167206777176090868L;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("primary")
	private Boolean primary;
	
	@JsonProperty("type")
	private String type;
	
	@Valid
	@JsonProperty("currency")
	private Currency currency;
	
	@Valid
	@JsonProperty("balance")
	private Balance balance;
	
	@JsonProperty("created_at")
	private Object createdAt;
	
	@JsonProperty("updated_at")
	private Object updatedAt;
	
	@JsonProperty("resource")
	private String resource;
	
	@JsonProperty("resource_path")
	private String resourcePath;
	
	@JsonProperty("allow_deposits")
	private Boolean allowDeposits;
	
	@JsonProperty("allow_withdrawals")
	private Boolean allowWithdrawals;
	
	@Valid
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Datum() {
	}

	/**
	 * 
	 * @param updatedAt
	 * @param id
	 * @param resourcePath
	 * @param balance
	 * @param allowDeposits
	 * @param createdAt
	 * @param primary
	 * @param name
	 * @param resource
	 * @param allowWithdrawals
	 * @param type
	 * @param currency
	 */
	public Datum(String id, String name, Boolean primary, String type, Currency currency, Balance balance,
			Object createdAt, Object updatedAt, String resource, String resourcePath, Boolean allowDeposits,
			Boolean allowWithdrawals) {
		super();
		this.id = id;
		this.name = name;
		this.primary = primary;
		this.type = type;
		this.currency = currency;
		this.balance = balance;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.resource = resource;
		this.resourcePath = resourcePath;
		this.allowDeposits = allowDeposits;
		this.allowWithdrawals = allowWithdrawals;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("primary")
	public Boolean getPrimary() {
		return primary;
	}

	@JsonProperty("primary")
	public void setPrimary(Boolean primary) {
		this.primary = primary;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("currency")
	public Currency getCurrency() {
		return currency;
	}

	@JsonProperty("currency")
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@JsonProperty("balance")
	public Balance getBalance() {
		return balance;
	}

	@JsonProperty("balance")
	public void setBalance(Balance balance) {
		this.balance = balance;
	}

	@JsonProperty("created_at")
	public Object getCreatedAt() {
		return createdAt;
	}

	@JsonProperty("created_at")
	public void setCreatedAt(Object createdAt) {
		this.createdAt = createdAt;
	}

	@JsonProperty("updated_at")
	public Object getUpdatedAt() {
		return updatedAt;
	}

	@JsonProperty("updated_at")
	public void setUpdatedAt(Object updatedAt) {
		this.updatedAt = updatedAt;
	}

	@JsonProperty("resource")
	public String getResource() {
		return resource;
	}

	@JsonProperty("resource")
	public void setResource(String resource) {
		this.resource = resource;
	}

	@JsonProperty("resource_path")
	public String getResourcePath() {
		return resourcePath;
	}

	@JsonProperty("resource_path")
	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	@JsonProperty("allow_deposits")
	public Boolean getAllowDeposits() {
		return allowDeposits;
	}

	@JsonProperty("allow_deposits")
	public void setAllowDeposits(Boolean allowDeposits) {
		this.allowDeposits = allowDeposits;
	}

	@JsonProperty("allow_withdrawals")
	public Boolean getAllowWithdrawals() {
		return allowWithdrawals;
	}

	@JsonProperty("allow_withdrawals")
	public void setAllowWithdrawals(Boolean allowWithdrawals) {
		this.allowWithdrawals = allowWithdrawals;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additionalProperties == null) ? 0 : additionalProperties.hashCode());
		result = prime * result + ((allowDeposits == null) ? 0 : allowDeposits.hashCode());
		result = prime * result + ((allowWithdrawals == null) ? 0 : allowWithdrawals.hashCode());
		result = prime * result + ((balance == null) ? 0 : balance.hashCode());
		result = prime * result + ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((primary == null) ? 0 : primary.hashCode());
		result = prime * result + ((resource == null) ? 0 : resource.hashCode());
		result = prime * result + ((resourcePath == null) ? 0 : resourcePath.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((updatedAt == null) ? 0 : updatedAt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Datum other = (Datum) obj;
		if (additionalProperties == null) {
			if (other.additionalProperties != null)
				return false;
		} else if (!additionalProperties.equals(other.additionalProperties))
			return false;
		if (allowDeposits == null) {
			if (other.allowDeposits != null)
				return false;
		} else if (!allowDeposits.equals(other.allowDeposits))
			return false;
		if (allowWithdrawals == null) {
			if (other.allowWithdrawals != null)
				return false;
		} else if (!allowWithdrawals.equals(other.allowWithdrawals))
			return false;
		if (balance == null) {
			if (other.balance != null)
				return false;
		} else if (!balance.equals(other.balance))
			return false;
		if (createdAt == null) {
			if (other.createdAt != null)
				return false;
		} else if (!createdAt.equals(other.createdAt))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (primary == null) {
			if (other.primary != null)
				return false;
		} else if (!primary.equals(other.primary))
			return false;
		if (resource == null) {
			if (other.resource != null)
				return false;
		} else if (!resource.equals(other.resource))
			return false;
		if (resourcePath == null) {
			if (other.resourcePath != null)
				return false;
		} else if (!resourcePath.equals(other.resourcePath))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (updatedAt == null) {
			if (other.updatedAt != null)
				return false;
		} else if (!updatedAt.equals(other.updatedAt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Datum [id=" + id + ", name=" + name + ", primary=" + primary + ", type=" + type + ", currency="
				+ currency + ", balance=" + balance + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt
				+ ", resource=" + resource + ", resourcePath=" + resourcePath + ", allowDeposits=" + allowDeposits
				+ ", allowWithdrawals=" + allowWithdrawals + ", additionalProperties=" + additionalProperties + "]";
	}

}
