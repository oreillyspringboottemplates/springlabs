
package com.oreillyauto.model.currency;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "code", "name", "color", "sort_index", "exponent", "type", "address_regex", "asset_id",
		"destination_tag_name", "destination_tag_regex" })
public class Currency implements Serializable {
	private final static long serialVersionUID = -3968411092997350850L;
	
	@JsonProperty("code")
	private String code;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("color")
	private String color;
	
	@JsonProperty("sort_index")
	private Integer sortIndex;
	
	@JsonProperty("exponent")
	private Integer exponent;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("address_regex")
	private String addressRegex;
	
	@JsonProperty("asset_id")
	private String assetId;
	
	@JsonProperty("destination_tag_name")
	private String destinationTagName;
	
	@JsonProperty("destination_tag_regex")
	private String destinationTagRegex;
	
	@Valid
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Currency() {
	}

	/**
	 * 
	 * @param sortIndex
	 * @param destinationTagRegex
	 * @param destinationTagName
	 * @param assetId
	 * @param color
	 * @param addressRegex
	 * @param exponent
	 * @param name
	 * @param code
	 * @param type
	 */
	public Currency(String code, String name, String color, Integer sortIndex, Integer exponent, String type,
			String addressRegex, String assetId, String destinationTagName, String destinationTagRegex) {
		super();
		this.code = code;
		this.name = name;
		this.color = color;
		this.sortIndex = sortIndex;
		this.exponent = exponent;
		this.type = type;
		this.addressRegex = addressRegex;
		this.assetId = assetId;
		this.destinationTagName = destinationTagName;
		this.destinationTagRegex = destinationTagRegex;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	@JsonProperty("code")
	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("color")
	public String getColor() {
		return color;
	}

	@JsonProperty("color")
	public void setColor(String color) {
		this.color = color;
	}

	@JsonProperty("sort_index")
	public Integer getSortIndex() {
		return sortIndex;
	}

	@JsonProperty("sort_index")
	public void setSortIndex(Integer sortIndex) {
		this.sortIndex = sortIndex;
	}

	@JsonProperty("exponent")
	public Integer getExponent() {
		return exponent;
	}

	@JsonProperty("exponent")
	public void setExponent(Integer exponent) {
		this.exponent = exponent;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("address_regex")
	public String getAddressRegex() {
		return addressRegex;
	}

	@JsonProperty("address_regex")
	public void setAddressRegex(String addressRegex) {
		this.addressRegex = addressRegex;
	}

	@JsonProperty("asset_id")
	public String getAssetId() {
		return assetId;
	}

	@JsonProperty("asset_id")
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	@JsonProperty("destination_tag_name")
	public String getDestinationTagName() {
		return destinationTagName;
	}

	@JsonProperty("destination_tag_name")
	public void setDestinationTagName(String destinationTagName) {
		this.destinationTagName = destinationTagName;
	}

	@JsonProperty("destination_tag_regex")
	public String getDestinationTagRegex() {
		return destinationTagRegex;
	}

	@JsonProperty("destination_tag_regex")
	public void setDestinationTagRegex(String destinationTagRegex) {
		this.destinationTagRegex = destinationTagRegex;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additionalProperties == null) ? 0 : additionalProperties.hashCode());
		result = prime * result + ((addressRegex == null) ? 0 : addressRegex.hashCode());
		result = prime * result + ((assetId == null) ? 0 : assetId.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((destinationTagName == null) ? 0 : destinationTagName.hashCode());
		result = prime * result + ((destinationTagRegex == null) ? 0 : destinationTagRegex.hashCode());
		result = prime * result + ((exponent == null) ? 0 : exponent.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((sortIndex == null) ? 0 : sortIndex.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Currency other = (Currency) obj;
		if (additionalProperties == null) {
			if (other.additionalProperties != null)
				return false;
		} else if (!additionalProperties.equals(other.additionalProperties))
			return false;
		if (addressRegex == null) {
			if (other.addressRegex != null)
				return false;
		} else if (!addressRegex.equals(other.addressRegex))
			return false;
		if (assetId == null) {
			if (other.assetId != null)
				return false;
		} else if (!assetId.equals(other.assetId))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (destinationTagName == null) {
			if (other.destinationTagName != null)
				return false;
		} else if (!destinationTagName.equals(other.destinationTagName))
			return false;
		if (destinationTagRegex == null) {
			if (other.destinationTagRegex != null)
				return false;
		} else if (!destinationTagRegex.equals(other.destinationTagRegex))
			return false;
		if (exponent == null) {
			if (other.exponent != null)
				return false;
		} else if (!exponent.equals(other.exponent))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sortIndex == null) {
			if (other.sortIndex != null)
				return false;
		} else if (!sortIndex.equals(other.sortIndex))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Currency [code=" + code + ", name=" + name + ", color=" + color + ", sortIndex=" + sortIndex
				+ ", exponent=" + exponent + ", type=" + type + ", addressRegex=" + addressRegex + ", assetId="
				+ assetId + ", destinationTagName=" + destinationTagName + ", destinationTagRegex="
				+ destinationTagRegex + ", additionalProperties=" + additionalProperties + "]";
	}
	
}
