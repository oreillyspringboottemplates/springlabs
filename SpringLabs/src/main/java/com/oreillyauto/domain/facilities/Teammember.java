package com.oreillyauto.domain.facilities;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="TEAMMEMBERS")
public class Teammember implements Serializable {
	private static final long serialVersionUID = -5996834027120348470L;
    public Teammember() {}
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "teammember_id", columnDefinition = "INTEGER")
    private Integer teammemberId;
    
    @Column(name = "first_name", columnDefinition = "VARCHAR(64)")
    private String firstName;
    
    @Column(name = "last_name", columnDefinition = "VARCHAR(64)")
    private String lastName;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "facility_id", referencedColumnName = "facility_id", columnDefinition = "INTEGER")
    private Facility facility;
    
	public Integer getTeammemberId() {
		return teammemberId;
	}

	public void setTeammemberId(Integer teammemberId) {
		this.teammemberId = teammemberId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	@Override
	public String toString() {
		return "Teammember [teammemberId=" + teammemberId + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}
    
}
