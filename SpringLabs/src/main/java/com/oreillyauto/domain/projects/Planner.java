package com.oreillyauto.domain.projects;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PLANNERS")
public class Planner { 
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "planner_id", columnDefinition = "INTEGER")
    private Integer plannerId;
	
    @Column(name = "first_name", columnDefinition = "VARCHAR(64)")
    private String firstName;
    
    @Column(name = "last_name", columnDefinition = "VARCHAR(64)")
    private String lastName;
    
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(
        name = "Planners_Projects", 
        joinColumns = {@JoinColumn(name = "planner_id")}, 
        inverseJoinColumns = {@JoinColumn(name = "project_id")}
    )
    Set<Project> projects = new HashSet<Project>();

	public Integer getPlannerId() {
		return plannerId;
	}

	public void setPlannerId(Integer plannerId) {
		this.plannerId = plannerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	@Override
	public String toString() {
		return "Planner [plannerId=" + plannerId + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}
    
}