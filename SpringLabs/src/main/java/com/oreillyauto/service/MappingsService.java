package com.oreillyauto.service;

import java.util.List;

import org.springframework.ui.Model;

import com.oreillyauto.domain.facilities.Facility;
import com.oreillyauto.domain.facilities.Teammember;
import com.oreillyauto.domain.interns.Intern;
import com.oreillyauto.domain.orders.Order;
import com.oreillyauto.domain.orders.OrderItem;
import com.oreillyauto.domain.projects.Planner;
import com.oreillyauto.domain.projects.Project;
import com.oreillyauto.domain.schools.School;
import com.oreillyauto.domain.schools.Student;

public interface MappingsService {
	public List<Order> getOrders();
	public List<OrderItem> getOrderItems();
	public List<School> getSchools();
	public List<Student> getStudents();
	public List<Facility> getFacilities();
	public List<Teammember> getTeammembers();
	public List<Object> getUserTables();
	public void addUserTablesToTheModel(Model model);
	public List<Planner> getPlanners();
	public List<Project> getProjects();
	public List<Intern> getInterns();
	public void saveIntern(Intern intern);
}
