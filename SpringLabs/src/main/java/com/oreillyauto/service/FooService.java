package com.oreillyauto.service;
import java.util.List;
import java.util.Map;

import com.oreillyauto.domain.foo.Clazz;
import com.oreillyauto.domain.foo.Pupil;

public interface FooService {
	public List<Pupil> getPupils();
	public List<Clazz> getClasses();
	public Map<String,Clazz> getClassesQdsl();
	public Pupil addPupil(Pupil pupil);
	public Integer getTransactionCount(Map<String, Clazz> clazzMap);
	public boolean isValid(Pupil pupil);
}
