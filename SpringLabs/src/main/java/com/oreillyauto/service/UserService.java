package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.User;

public interface UserService {
	public List<User> getUsers();
	public User getUserById(Integer idInt);
	public void saveUser(User user);
	public void deleteUser(Integer userId);
}
