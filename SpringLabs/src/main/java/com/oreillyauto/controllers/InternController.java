package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.oreillyauto.service.InternService;
import com.twilio.rest.video.v1.Room;

@RestController
public class InternController {
    @Autowired
    InternService internService;

    /*
     *  Add a mapping here that returns a List<Intern> and takes in a String name
     *  
     *  You'll see 404 in the tests with the mapping 
     *  
     *  Don't forget to replaceAll("\"","")
     *  
     */  
    
    //DO NOT Change BELOW
    @PostMapping("/interns/lastName")
    @ResponseStatus(HttpStatus.OK)
    public List<Room> getInternByLastName(@RequestBody String lastName) {
        lastName = lastName.replaceAll("\"", "");
        return internService.findInternsWithLastName(lastName);
    }
}

