package com.oreillyauto.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.InternRepositoryCustom;
import com.oreillyauto.domain.interns.Intern;

public interface InternRepository extends CrudRepository<Intern, Integer>, InternRepositoryCustom {
	
/*	@Query(value = "SELECT firstName FROM interns", nativeQuery = true)
    public List<Object[]> findFirstName();*/
}
