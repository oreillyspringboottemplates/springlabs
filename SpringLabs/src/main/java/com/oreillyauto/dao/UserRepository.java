package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.UserRepositoryCustom;
import com.oreillyauto.domain.User;

public interface UserRepository extends CrudRepository<User, Integer>, UserRepositoryCustom {
    
    // Add your interface methods here (that ARE Spring Data methods)
    
}
