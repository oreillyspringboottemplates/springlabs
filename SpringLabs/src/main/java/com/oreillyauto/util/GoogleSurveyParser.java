package com.oreillyauto.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class GoogleSurveyParser {

    public GoogleSurveyParser() throws Exception {
        List<String> lineList = getSurveyFromFile();
        int lineCount = 0;
        List<String> questionList = new ArrayList<String>();
        List<Respondent> respondentList = new ArrayList<Respondent>();
        
        for (String line : lineList) {
            if (lineCount == 0) {
                String[] tokens = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
                
                for (int i = 1; i < tokens.length; i++) {
                    questionList.add(tokens[i].replace("\"", ""));
                }
                
            } else {
                Respondent respondent = new Respondent();
                respondent.setRespondent(lineCount);
                
                String[] answers = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
                
                for (int i = 0; i < answers.length; i++) {
                    if (i == 0) {
                        respondent.setTimestamp(answers[i].replace("\"", "")); 
                    } else {
                        respondent.addAnswer(answers[i].replace("\"", ""));    
                    }
                }
                
                respondentList.add(respondent);
            }
            
            lineCount += 1;
            
        }
        
        //printRespondents(respondentList);
        
        //int questionCounter = 0;
        
        System.out.println("QUESTION,R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15");
        
        for (int questionCounter = 0; questionCounter < 9; questionCounter++) {
            String question = questionList.get(questionCounter);
            
            // Print the question
            System.out.print(question + ",");
            
            for (Respondent respondent: respondentList) {
                // Get the answer for each respondent at questionCounter 
                List<String> answerList = respondent.getAnswerList();
                String answer = answerList.get(questionCounter);
                System.out.print(answer + ",");
            }
         
            System.out.println("");
        }
        
        
        // Print results
/*        for (String question: questionList) {
            System.out.print(question + ",");
            Respondent respondent = respondentList.get(questionCounter-1);
            List<String> answerList = respondent.getAnswerList();
            
            for (int j = 0; j < 9; j++) {
                System.out.print(answerList.get(j) + ",");
            }
            
            questionCounter += 1;
            System.out.println("");
        }*/
        
        System.out.println("");
        System.out.println("");
        
        // Print open answers
        for (Respondent respondent : respondentList) {
            System.out.println("R" + respondent.getRespondent() + "," + respondent.getAnswerList().get(9));
        }
        
    }
    
    private void printRespondents(List<Respondent> respondentList) {
        for (Respondent respondent : respondentList) {
            System.out.println(respondent.getRespondent() + " Timestamp: " + respondent.getTimestamp() + " : answerList size = "
                    + respondent.getAnswerList().size());
        }
    }

    private List<String> getSurveyFromFile() throws Exception {
        String line;
        List<String> lineList = new ArrayList<String>();
        BufferedReader 
        br = null;
        File file = new File("/Users/jbrannon5/Downloads/SpringMVC Week 3 - Spring 19.csv/SpringMVC Week 3 - Spring 19.csv");
        
        try {
            br = new BufferedReader(new FileReader(file));
               
            while ((line = br.readLine()) != null) { 
              lineList.add(line); 
            }
        } finally {
            try { br.close(); }catch(Exception ex) {/* do nothing*/}
        }
        
        return lineList;
    }

    public static void main(String[] args) {
        try {
            new GoogleSurveyParser();    
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
