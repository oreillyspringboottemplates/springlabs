package com.oreillyauto.util;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DynamicDeserialization {

	public DynamicDeserialization() throws Exception {
		
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("  \"1\": \"One\",");
		sb.append("  \"2\": \"Two\",");
		sb.append("  \"3\": \"Three\",");
		sb.append("  \"4\": \"Four\",");
		sb.append("  \"5\": \"Five\",");
		sb.append("  \"6\": \"Six\"");
		sb.append("}");

		ObjectMapper mapper = new ObjectMapper();
		
		Map<String, Object> map = 
				mapper.readValue(sb.toString(), new TypeReference<Map<String, Object>>() {});
		
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		}
	}

	
	public static void main(String[] args) throws Exception {
		new DynamicDeserialization();
	}
}

