package com.oreillyauto.util;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oreillyauto.model.currency.Currency;
import com.oreillyauto.model.currency.Datum;
import com.oreillyauto.model.currency.Response;


public class CurrencyTest {

	public CurrencyTest() throws Exception {
		String json = "{\n\t\"data\": [\n\t\t{\n\t\t\t\"id\": \"4ba4c782-1054-505c-8e20-13e4766b6b19\",\n\t\t\t\"name\": \"BTC Wallet\",\n\t\t\t\"primary\": true,\n\t\t\t\"type\": \"wallet\",\n\t\t\t\"currency\": {\n\t\t\t\t\"code\": \"BTC\",\n\t\t\t\t\"name\": \"Bitcoin\",\n\t\t\t\t\"color\": \"#F7931A\",\n\t\t\t\t\"sort_index\": 100,\n\t\t\t\t\"exponent\": 8,\n\t\t\t\t\"type\": \"crypto\",\n\t\t\t\t\"address_regex\": \"^([13][a-km-zA-HJ-NP-Z1-9]{25,34})|^(bc1([qpzry9x8gf2tvdw0s3jn54khce6mua7l]{39}|[qpzry9x8gf2tvdw0s3jn54khce6mua7l]{59}))$\",\n\t\t\t\t\"asset_id\": \"5b71fc48-3dd3-540c809b-f8c94d0e68b5\"\n\t\t\t},\n\t\t\t\"balance\": {\n\t\t\t\t\"amount\": \"0.00075652\",\n\t\t\t\t\"currency\": \"BTC\"\n\t\t\t},\n\t\t\t\"created_at\": \"2019-04-15T20:04:12Z\",\n\t\t\t\"updated_at\": \"2019-04-22T01:35:06Z\",\n\t\t\t\"resource\": \"account\",\n\t\t\t\"resource_path\": \"/v2/accounts/4ba4c782-1054-505c-8e20-13e4766b6b19\",\n\t\t\t\"allow_deposits\": true,\n\t\t\t\"allow_withdrawals\": true\n\t\t},\n\t\t{\n\t\t\t\"id\": \"BCH\",\n\t\t\t\"name\": \"BCH Wallet\",\n\t\t\t\"primary\": true,\n\t\t\t\"type\": \"wallet\",\n\t\t\t\"currency\": {\n\t\t\t\t\"code\": \"BCH\",\n\t\t\t\t\"name\": \"Bitcoin Cash\",\n\t\t\t\t\"color\": \"#8DC351\",\n\t\t\t\t\"sort_index\": 101,\n\t\t\t\t\"exponent\": 8,\n\t\t\t\t\"type\": \"crypto\",\n\t\t\t\t\"address_regex\": \"^([13][a-km-zA-HJ-NP-Z1-9]{25,34})|^((bitcoincash:)?(q|p)[a-z0-9]{41})|^((BITCOINCASH:)?(Q|P)[A-Z0-9]{41})$\",\n\t\t\t\t\"asset_id\": \"45f99e13-b522-57d7-8058-c57bf92fe7a3\"\n\t\t\t},\n\t\t\t\"balance\": {\n\t\t\t\t\"amount\": \"0.00000000\",\n\t\t\t\t\"currency\": \"BCH\"\n\t\t\t},\n\t\t\t\"created_at\": null,\n\t\t\t\"updated_at\": null,\n\t\t\t\"resource\": \"account\",\n\t\t\t\"resource_path\": \"/v2/accounts/BCH\",\n\t\t\t\"allow_deposits\": true,\n\t\t\t\"allow_withdrawals\": true\n\t\t},\n\t\t{\n\t\t\t\"id\": \"XLM\",\n\t\t\t\"name\": \"XLM Wallet\",\n\t\t\t\"primary\": true,\n\t\t\t\"type\": \"wallet\",\n\t\t\t\"currency\": {\n\t\t\t\t\"code\": \"XLM\",\n\t\t\t\t\"name\": \"Stellar Lumens\",\n\t\t\t\t\"color\": \"#000000\",\n\t\t\t\t\"sort_index\": 127,\n\t\t\t\t\"exponent\": 7,\n\t\t\t\t\"type\": \"crypto\",\n\t\t\t\t\"address_regex\": \"^G[A-Z2-7]{55}$\",\n\t\t\t\t\"asset_id\": \"13b83335-5ede-595b-821e-5bcdfa80560f\",\n\t\t\t\t\"destination_tag_name\": \"XLM Memo\",\n\t\t\t\t\"destination_tag_regex\": \"^[ -~]{1,28}$\"\n\t\t\t},\n\t\t\t\"balance\": {\n\t\t\t\t\"amount\": \"0.0000000\",\n\t\t\t\t\"currency\": \"XLM\"\n\t\t\t},\n\t\t\t\"created_at\": null,\n\t\t\t\"updated_at\": null,\n\t\t\t\"resource\": \"account\",\n\t\t\t\"resource_path\": \"/v2/accounts/XLM\",\n\t\t\t\"allow_deposits\": true,\n\t\t\t\"allow_withdrawals\": true\n\t\t}\n\t],\n\t\"warnings\": [\n\t\t{\n\t\t\t\"id\": \"missing_version\",\n\t\t\t\"message\": \"Please supply API version (YYYY-MM-DD) as CB-VERSION header\",\n\t\t\t\"url\": \"https://developers.coinbase.com/api#versioning\"\n\t\t}\n\t]\n}";
		ObjectMapper mapper = new ObjectMapper();
		Response response = mapper.readValue(json, Response.class);
		List<Datum> datumList = response.getData();
		
		StringBuilder sb = null;
		
		for (Datum datum : datumList) {
			sb = new StringBuilder("Coin Information=> ");
			sb.append("Datum Name:" + datum.getName() + " ");
			Currency currency = datum.getCurrency();
			sb.append("Currency Name:"+ currency.getName() + " ");
			sb.append("Type:"+currency.getType() + " ");
			sb.append("Balance:"+ datum.getBalance().getAmount());
			System.out.println(sb.toString());
		}
	}

	public static void main(String[] args) throws Exception {
		new CurrencyTest();
	}

}
