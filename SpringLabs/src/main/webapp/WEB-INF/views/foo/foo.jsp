<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Kung Foo LLC</h1>
<h2>Add Student</h2>
<div class="card">
	<div class="card-body">
		<form>
			<div class="col-sm-4 form-group">
				<label for="txId">ID</label>
				<input type="text" placeholder="ID" name="txId" id="txId" class="form-control" disabled />
			</div>
			<div class="col-sm-4 form-group">
				<label for="firstName">First Name</label>
				<input type="text" placeholder="First Name" name="firstName" id="firstName" class="form-control" />
			</div>
			<div class="col-sm-4 form-group">
				<label for="lastName">Last Name</label>
				<input type="text" placeholder="Last Name" name="lastName" id="lastName" class="form-control" />
			</div>
			<div class="col-sm-6 form-group">
				<label for="enrollDate" class="mt10">Enroll Date</label>
				<orly-datepicker id="enrollDateString" name="enrollDateString" value="" size="sm"></orly-datepicker>
				<!-- <orly-datepicker id="enrollDate" name="enrollDate" value="" size="sm"></orly-datepicker> -->
			</div>
			<div class="col-sm-6 form-group">
				<label for="clazz">Select Class</label>
				<select class="form-control" id="clazz" name="clazz">
					<c:forEach items="${clazzList}" var="clazz">
						<option value="${clazz.classId}">${clazz.className} ${clazz.days}</option>
					</c:forEach>
				</select>
			</div>
			<div class="col-sm-12 form-group">
				<button id="submitBtn" type="button" class="btn btn-primary">Submit</button>
			</div>
		</form>
	</div>
</div>

<hr/>

<h2>Students</h2>
<c:set value="${fn:length(pupilListJson) gt 0 ? pupilListJson : []}" var="tableData"/>
<orly-table id="pupilTable" loaddataoncreate includefilter bordered maxrows="10" tabletitle="Search Results" class="invisible" data='${tableData}'>
	<orly-column field="txId" label="Transaction Id" class="" sorttype="natural"></orly-column>
	<orly-column field="firstName" label="First Name"></orly-column>
	<orly-column field="lastName" label="Last Name"></orly-column>
	<orly-column field="enrollDate" label="Enroll Date"></orly-column>
</orly-table>

<script>	
	var pupilTable;

	orly.ready.then(() => {
		pupilTable = orly.qid("pupilTable");
		
		pupilTable.updateColumn("enrollDate", "cellFn", function(row, cell, item) { 			
			return orly.formatDate(new Date(item.enrollDate), 'MM/DD/YYYY');
		});
		
		orly.qid("submitBtn").addEventListener("click", function(e){
			try {
				e.preventDefault(); // Prevent Form From Submitting "normally" (IE)
				let Pupil = {};
				Pupil.firstName = orly.qid("firstName").value 
				Pupil.lastName = orly.qid("lastName").value;
				Pupil.enrollDateString = orly.qid("enrollDateString").value;
				
				console.log("typeof enrollDateString" + typeof enrollDateString);
				
				Pupil.classId = orly.qid("clazz").value;
				
				// Make AJAX call to the server
				<%-- fetch("<%=request.getContextPath()%>/foo/addPupil'", {} --%>
				/* fetch("${pageContext.request.contextPath}/foo/addPupil'", {} */
						
				fetch("<c:url value='/foo/addPupil' />", {
				        method: "POST",
				        body: JSON.stringify(Pupil),
				        headers: {
				            "Content-Type": "application/json"
				        }
				}).then(function(response) { // Call .then() to get the instance of the HttpServletResponse
				  	if (response.ok) {  // Check the http status code
				  		console.log("response.status=", response.status);
				  		let message = response.json(); // call the json function in the Fetch API to "extract" the JSON... BUT it returns a PROMISE
				  		console.log("message = " + message);
				  		return message;
				  	} else {
				  		throw new Error("Error: " + response.statusText);
				  	}
				}).then(function(Response) { // Call .then() so we can convert the Promise to a JSON Object 
					//let Message = JSON.parse(response);
					let message = Response.message;
					let messageType = Response.messageType;
					
					if (messageType == null || messageType == "undefined" || messageType.length == 0) {
						messageType = "info";
					}
				  	
				  	if (message != null && message != "undefined" && message.length > 0) {
				  		orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});	
				  	}
				  	
				  	populateTableWithResponse(Response);
				  	
				}).catch(function(error) {
					let message = 'There was a problem with your fetch operation: ' + error.message;
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:message});
				});
		
			} catch (err) {
				// Do not show try-catch errors to the user in production
				orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:err});
			}
		});
	});
	
	function populateTableWithResponse(Response) {
		try {
			let pupilList = Response.pupilList;
			
			if (pupilList != null && pupilList != "undefined") {
				pupilTable.data = pupilList;
			} else {
				console.log("Unable to populate table with new user list");
			}	
		} catch (e) {
			console.log("Unable to populate table with new user list");	
		}
	}
</script>