

<h1>Example Table</h1>
<orly-table id="exampleTable" loaddataoncreate includefilter bordered maxrows="10" 
            tabletitle="Example Table Results" class="invisible" data=''>
	<orly-column field="levelTwo" label="levelTwo">
		<div slot="cell">
			\${model.levelOne.levelTwoA}
		</div>
	</orly-column>
<%-- 	<orly-column field="levelThree" label="levelTwoA">
		<div slot="cell">
			\${model.levelTwo.levelTwoA}
		</div>
	</orly-column> --%>
</orly-table>
<div class="row">
	<div class="col-sm-3 form-group">
		<button id="fetchBtn" type="button" class="btn btn-primary">Fetch</button>
	</div>
</div>

<script>	
	var exampleTable;

	orly.ready.then(() => {
		exampleTable = orly.qid("exampleTable");
		
		orly.qid("fetchBtn").addEventListener("click", function(e){
			try {
				e.preventDefault();
				
				// Make AJAX call to the server
				fetch("<c:url value='/example/tableExampleData' />", {
				        method: "GET",
				        headers: {
				            "Content-Type": "application/json"
				        }
				}).then(function(response) {
				  	if (response.ok) {
				  		console.log("response.status=", response.status);
				  		let message = response.json();
				  		console.log("message = " + message);
				  		return message
				  	} else {
				  		throw new Error("Error: " + response.statusText);
				  	}
				}).then(function(Response) {
					let response = JSON.parse(Response);			  	
					exampleTable.data = response;
				}).catch(function(error) {
					let message = 'There was a problem with your fetch operation: ' + error.message;
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:message});
				});
		
			} catch (err) {
				// Do not show try-catch errors to the user in production
				orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:err});
			}
		});
	});
</script>