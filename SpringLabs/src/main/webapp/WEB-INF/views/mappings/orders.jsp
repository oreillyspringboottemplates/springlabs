<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Unidirectional Mappings</h1>
<h3>Unidirectional Many-to-One</h3>
<hr/>
<h4>Orders</h4>
<p>
Since we have a unidirectional child mapping from orderItems to
orders, we cannot query orderItems in an Order entity without
throwing an exception. There is no property "orderItemList"
on the Order domain entity!
</p>
<c:forEach items="${orderList}" var="order">
	<div><strong>* ${order}</strong></div>
	<%--
		Since we have a unidirectional mapping from orderItems to
		orders, we cannot query orderItems in an Order without
		throwing an exception. There is no property "orderItemList"
		on the property domain entity!
	 --%>
<%-- 	<c:forEach items="${order.orderItemList}" var="item">	
		<div>** ${item}</div>
	</c:forEach> --%>
</c:forEach>

<hr/>

<h4>Order Items</h4>
<c:forEach items="${orderItemList}" var="itemList">
	<div><strong>* ${itemList}</strong></div>
	<div>** ${itemList.order}</div>
</c:forEach>

<hr/>

<h4>User Tables</h4>
<c:forEach items="${userTableList}" var="table">
	${table}<br/>
</c:forEach>

<script>	
	orly.ready.then(() => {
	});
</script>
